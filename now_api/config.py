from pydantic import BaseSettings


class Settings(BaseSettings):
    app_name: str = "NOW"
    database_url: str
    root_password: str = None


settings = Settings()
