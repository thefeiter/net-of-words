from fastapi import Depends, HTTPException, Header, Cookie, status
from fastapi.security import HTTPBasicCredentials, HTTPBasic
from .dal.user import get_user_auth_by_name, get_user_by_id

from .models.user import User, UserAuth
from .db import tbl_users, database
from datetime import datetime, timedelta

import hashlib
from sqlalchemy import select, and_, func
import secrets


async def check_user(username: str, password_hash: str) -> User:

    userAuth: UserAuth = await get_user_auth_by_name(username)

    if not userAuth:
        raise HTTPException(status.HTTP_401_UNAUTHORIZED,
                            detail="Nutzer existiert nicht")
    elif not secrets.compare_digest(password_hash, userAuth.password_hash):
        raise HTTPException(status.HTTP_401_UNAUTHORIZED,
                            detail="Nutzername oder Passwort falsch")

    return userAuth.toUser()


async def check_basic_user(username: str, password: str) -> User:
    digest = hashlib.sha256(password.encode("UTF-8"))
    request_pass_hash = digest.hexdigest()

    return await check_user(username, request_pass_hash)


async def generate_token(user_id):

    auth_token = secrets.token_urlsafe(nbytes=32)

    stmt = tbl_users.update().where(tbl_users.c.user_id == user_id).values(
        auth_token=auth_token,
        auth_token_valid=datetime.now()+timedelta(days=10)
    )
    update_count = await database.execute(stmt)

    return auth_token


async def update_token_valid_ts(user_id):
    stmt = tbl_users.update().where(tbl_users.c.user_id == user_id).values(
        auth_token_valid=datetime.now()+timedelta(days=10)
    )
    update_count = await database.execute(stmt)



async def get_token(user_id):

    stmt = select([tbl_users.c.auth_token]).where(
        and_(
            tbl_users.c.user_id == user_id,
            tbl_users.c.auth_token_valid > datetime.now()
        )
    )
    db_token = await database.fetch_one(stmt)

    if db_token:
        return db_token.auth_token
    else:
        return await generate_token(user_id)


async def check_token(token: str) -> User:

    if not token or token == "":
        return None

    stmt = select([tbl_users.c.user_id]).where(
        and_(
            tbl_users.c.auth_token == token,
            tbl_users.c.auth_token_valid > func.now()
        )
    )
    db_user = await database.fetch_one(stmt)

    if db_user:
        await update_token_valid_ts(db_user.user_id)
        user = await get_user_by_id(db_user.user_id)
    else:
        user = None

    return user


async def session_token_auth(authorization: str | None = Header(default=None)):
    
    if not authorization:
        return None

    token = authorization.split(" ")
    token = token[1] if len(token)-1 else None

    return await check_token(token)


"""async def cookie_auth(access_token: str | None = Cookie(default=None)):

    return await check_token(access_token)"""


async def basic_auth(credentials: HTTPBasicCredentials = Depends(HTTPBasic(auto_error=False))) -> User:

    if not credentials:
        return None

    return await check_basic_user(credentials.username, credentials.password)


# token_user=Depends(session_token_auth), cookie_user=Depends(cookie_auth),
# token_user or cookie_user or
async def authentication(basic_user: User = Depends(basic_auth), token_user=Depends(session_token_auth)):
    if not (basic_user or token_user):
        raise HTTPException(status.HTTP_401_UNAUTHORIZED)

    '''if token_user:
        return token_user'''

    if basic_user:
        return basic_user

    if token_user:
        return token_user
