from now_api.endpoints.vocabs import create_new_vocab, create_new_vocab_vocab_relation
from now_api.models.vocabulary import VocabIn
from ..endpoints.vocabs import get_all_vocabs
from .. import app
import random
from pymysql import IntegrityError

async def get_all_category_relations():
    from ..db import database, tbl_cat_relations, tbl_vocabs, tbl_categories
    query = tbl_cat_relations.select()
    return await database.fetch_all(query)


async def migrate_c_to_v():
    from ..db import database, tbl_cat_relations, tbl_vocabs, tbl_categories
    categories = await database.fetch_all(tbl_categories.select())
    category_relations = await get_all_category_relations()


    c_v_map = {}

    for cat in categories:
        print(cat)

        #add new voc
        try:
            vocab = await create_new_vocab(vocab=VocabIn(primary=cat[1], secondary=cat[2]), user=None, is_category=True)
            c_v_map[cat[0]]=vocab.vocab_id
        except Exception as e:
            print(e)
            


    for c_rel in category_relations:
        print(c_rel)
        v_id=c_rel[1]
        c_id=c_rel[2]

        v2_id = c_v_map[c_id]

        
        try:
            await create_new_vocab_vocab_relation(vocab_id=v_id, related_vocab_id=v2_id, user=True)
        except Exception as e:
            print(e)
            
        #add v_v relation

    print(c_v_map)
    print(category_relations)