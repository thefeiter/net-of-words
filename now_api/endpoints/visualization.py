from .. import app


@app.get("/relnet/")
async def generate_relation_networks():

    await generate_relation_network(secondary=True)
    await generate_relation_network(secondary=False)

    return "OK"



async def generate_relation_network(secondary: bool = True):
    from pyvis.network import Network
    from ..db import database
    from sqlalchemy import text

    nt = Network()

    # vocabs
    query = text("""SELECT 
	vr.vocab_id, 
    (SELECT `primary` FROM vocabulary WHERE vocab_id=vr.vocab_id) AS `primary`,
    (SELECT secondary FROM vocabulary WHERE vocab_id=vr.vocab_id) AS secondary,
    vr.related_vocab_id, 
    (SELECT `primary` FROM vocabulary WHERE vocab_id=vr.related_vocab_id) AS related_primary,
    (SELECT secondary FROM vocabulary WHERE vocab_id=vr.related_vocab_id) AS related_secondary,
    (SELECT is_category FROM vocabulary WHERE vocab_id=vr.vocab_id) AS is_category,
    (SELECT is_category FROM vocabulary WHERE vocab_id=vr.related_vocab_id) AS related_is_category
FROM `vocab_relations` AS vr""")
    fetch = await database.fetch_all(query)

    for row in fetch:

        v1_nid="%s-%d-%s-%s" % ("c" if row[6] else "v", row[0], row[1], row[2])
        v2_nid="%s-%d-%s-%s" % ("c" if row[7] else "v", row[3], row[4], row[5])

        nt.add_node(v1_nid, label=row[2] if secondary else row[1], size=15)
        nt.add_node(v2_nid, label=row[5] if secondary else row[4], size=15)
        nt.add_edge(v1_nid, v2_nid)

    #neighbor_map = nt.get_adj_list()
    for node in nt.node_map:  # nodes hübsch machen
        nid=node.split("-")
        '''neighbours = list(neighbor_map[node])
        title = str(len(neighbours)) + ' Related:\n'
        for nb in neighbours:
            title += nt.node_map[nb]['label']+"\n"'''

        nt.node_map[node]['title'] = nid[-2] if secondary else nid[-1] 

        if str(node).startswith("c-"):
            nt.node_map[node]['color'] = "#ff0000"

    # physik einstellen
    # nt.show_buttons(filter_=['physics'])

    nt.set_options('''
        var options = {
        "physics": {
            "barnesHut": {
            "gravitationalConstant": -1250,
            "springConstant": 0.03,
            "centralGravity": 0.2,
            "springLength": 65
            },
            "minVelocity": 0.75,
            "timestep": 1.5
        }
        }
    ''')



    filename = "generated_html/relations-%s.html" % ("secondary" if secondary else "primary")

    # netzwerk speichern
    '''nt.save_graph(filename)
    # <div class="card" style="width: 100%">
    with open(filename, "r") as file:
        cont = file.read()'''

    cont = nt.generate_html(local=True)
    cont = cont.replace('https://cdnjs.cloudflare.com/ajax/libs/vis-network/9.1.2/dist/dist',
                        '.')
    cont = cont.replace('https://cdnjs.cloudflare.com/ajax/libs/vis-network/9.1.2/dist',
                        '.')
    cont = cont.replace('https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css',
                        '.')
    cont = cont.replace('https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js',
                        '.')
    cont = cont.replace('<div class="card" style="width: 100%">',
                        '<div class="card" style="width: 100%; height: 100%">')

    with open(filename, "w") as file:
        file.write(cont)
