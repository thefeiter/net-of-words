
from now_api.models.search import SearchResult
from .. import app
from ..db import tbl_vocabs, database
from sqlalchemy import or_

async def bicompare(query, data):

    if data is None or len(query) < 2 or len(data) < 2:
        return 0
        
    query_bigrams = []

    for i in range(len(query) - 1):
        query_bigrams.append(query.lower()[i:i + 2])

    data_bigrams = []

    for i in range(len(data) - 1):
        data_bigrams.append(data.lower()[i:i + 2])

    score = 0

    for ele in query_bigrams:
        if ele in data_bigrams:
            score += 1

    score = score / len(data_bigrams)

    return score


async def tricompare(query, data):
    if data is None:
        return 0

    if len(query) < 3 or len(data) < 3:
        return await bicompare(query, data)
        
    query_trigrams = []

    for i in range(len(query) - 2):
        query_trigrams.append(query.lower()[i:i + 3])

    data_trigrams = []

    for i in range(len(data) - 2):
        data_trigrams.append(data.lower()[i:i + 3])

    score = 0

    for ele in query_trigrams:
        if ele in data_trigrams:
            score += 1

    score = score / len(data_trigrams)

    return score


@app.get("/search/", response_model=SearchResult)
async def search_vocabs(query:str):

    # search vocabs
    stmt = tbl_vocabs.select().where(
        or_(
            tbl_vocabs.c.secondary.like("%{}%".format(query)),
            tbl_vocabs.c.primary.like("%{}%".format(query))
        ))
    vocabs = await database.fetch_all(stmt)

    vocab_sortlist=[]

    for vocab in vocabs:
        score = max(
            await tricompare(query, vocab.primary),
            await tricompare(query, vocab.secondary),) * 100

        vocab_sortlist.append(dict(vocab=vocab, score=score))

    vocab_sortlist = sorted(vocab_sortlist, key=lambda ele: ele.get("score"), reverse=True)


    return SearchResult(
        query=query,
        vocabs=[ele.get("vocab") for ele in vocab_sortlist])