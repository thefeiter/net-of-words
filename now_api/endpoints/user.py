from fastapi import Depends, HTTPException, status
from now_api.authentication import authentication
from now_api.dal.user import add_user

from .. import app
from ..models.user import User, UserIn, UserToken
from ..authentication import check_basic_user, check_user, get_token


@app.post("/user/", response_model=User)
async def create_new_user(new_user: UserIn, user: User = Depends(authentication)):

    if user.isAdmin:
        return await add_user(new_user)
    else:
        raise HTTPException(status.HTTP_403_FORBIDDEN)

@app.get("/user/", response_model=User)
async def who_am_i(user: User = Depends(authentication)):

    return user

@app.get("/user/token/")
async def get_auth_token(username:str, password_hash: str) -> str:

    user = await check_user(username, password_hash)

    return await get_token(user.user_id)
