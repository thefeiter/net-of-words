from enum import Enum

from fastapi import Depends, HTTPException, status
from now_api.authentication import authentication
from now_api.models.relations import VocabRelation
from now_api.models.user import User
from pymysql import IntegrityError
from pymysql.constants import ER
from sqlalchemy import and_, func, select, union

from .. import app
from ..db import (database, tbl_vocab_relations, tbl_vocabs)
from ..models.vocabulary import Vocab, VocabBatchResult, VocabDetails, VocabIn


@app.get("/vocabs/", response_model=list[Vocab])
async def get_all_vocabs(only_categories: bool = False):
    if only_categories:
        query = tbl_vocabs.select().where(tbl_vocabs.c.is_category == True)
    else:
        query = tbl_vocabs.select()
    return await database.fetch_all(query)


@app.post("/vocabs/", response_model=Vocab | VocabBatchResult)
async def create_new_vocab(vocab: VocabIn, user: User = Depends(authentication)):

    if vocab.primary == "":
        raise HTTPException(status.HTTP_400_BAD_REQUEST, detail="Primary cannot be empty")
    if vocab.secondary == "":
        raise HTTPException(status.HTTP_400_BAD_REQUEST, detail="Secondary cannot be empty")

    try:
        query = tbl_vocabs.insert().values(**vocab.dict())
        last_record_id = await database.execute(query)
        return Vocab(vocab_id=last_record_id, **vocab.dict())
    except IntegrityError:
        raise HTTPException(status.HTTP_409_CONFLICT, detail="This combination already exists")


@app.post("/batch/vocabs/", response_model=VocabBatchResult)
async def create_multiple_new_vocabs(vocabs: list[VocabIn], user: User = Depends(authentication)):

    conflict_vocabs = []

    success_vocabs = []

    for vocab in vocabs:
        try:
            success_vocabs.append(await create_new_vocab(vocab, user))
        except HTTPException as e:
            if e.status_code == status.HTTP_409_CONFLICT:
                conflict_vocabs.append(vocab)
            if e.status_code == status.HTTP_400_BAD_REQUEST:
                conflict_vocabs.append(vocab)
            else:
                raise e

    return VocabBatchResult(
        sucess_count=len(success_vocabs),
        conflict_count=len(conflict_vocabs),
        success_vocabs=success_vocabs,
        conflict_vocabs=conflict_vocabs
    )


@app.delete("/vocabs/{vocab_id}/", response_model=int)
async def delete_vocab(vocab_id: int, user: User = Depends(authentication)):

    try:
        query = tbl_vocabs.delete().where(tbl_vocabs.c.vocab_id == vocab_id)
        rowcount = await database.execute(query)
        return rowcount
    except IntegrityError:
        raise HTTPException(status.HTTP_409_CONFLICT, detail="This combination already exists")


@app.post("/vocabs/{vocab_id}/relations/{related_vocab_id}/", response_model=VocabRelation)
async def create_new_vocab_vocab_relation(vocab_id: int, related_vocab_id: int, user: User = Depends(authentication)):

    if vocab_id == related_vocab_id:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, detail="Cannot relate to oneself")

    try:
        query = tbl_vocab_relations.insert().values(vocab_id=vocab_id, related_vocab_id=related_vocab_id)
        last_record_id = await database.execute(query)
        return VocabRelation(rel_id=last_record_id, vocab_id=vocab_id, related_vocab_id=related_vocab_id)
    except IntegrityError as e:
        if e.args[0] == ER.DUP_ENTRY:
            raise HTTPException(status.HTTP_409_CONFLICT, detail="This relation already exists")
        elif e.args[0] == ER.NO_REFERENCED_ROW_2:
            raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Vocab not found")
        else:
            raise e


class SpecialVocabKeywords(str, Enum):
    random = "random"
    random_unrelated = "random_unrelated"


async def get_special_vocab_id(keyword: SpecialVocabKeywords):
    if keyword == SpecialVocabKeywords.random:
        query = tbl_vocabs.select().order_by(func.rand())
        vocab = await database.fetch_one(query)
    elif keyword == SpecialVocabKeywords.random_unrelated:

        query = tbl_vocabs.select()\
            .where(
                and_(
                    tbl_vocabs.c.vocab_id.notin_(select([tbl_vocab_relations.c.vocab_id])),
                    tbl_vocabs.c.vocab_id.notin_(select([tbl_vocab_relations.c.related_vocab_id])),
                )
        )\
            .order_by(func.rand())
        vocab = await database.fetch_one(query)

    if not vocab:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return vocab.vocab_id


@app.get("/vocabs/{vocab_id_or_keyword}/", response_model=VocabDetails)
async def get_vocab(vocab_id_or_keyword: int | SpecialVocabKeywords):

    if type(vocab_id_or_keyword) == int:
        vocab_id = vocab_id_or_keyword
    elif type(vocab_id_or_keyword) == SpecialVocabKeywords:
        vocab_id = await get_special_vocab_id(vocab_id_or_keyword)

    query_vocabs = tbl_vocabs.select().where(tbl_vocabs.c.vocab_id == vocab_id)
    vocab = await database.fetch_one(query_vocabs)

    if not vocab:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="vocab not found")

    # load vocabs
    join_a = tbl_vocab_relations.join(
        tbl_vocabs,
        tbl_vocab_relations.c.related_vocab_id == tbl_vocabs.c.vocab_id
    )
    query_a = tbl_vocabs.select().select_from(join_a).where(tbl_vocab_relations.c.vocab_id == vocab_id)

    join_b = tbl_vocab_relations.join(
        tbl_vocabs,
        tbl_vocab_relations.c.vocab_id == tbl_vocabs.c.vocab_id
    )
    query_b = tbl_vocabs.select().select_from(join_b).where(tbl_vocab_relations.c.related_vocab_id == vocab_id)

    query_vocabs = union(query_a, query_b)

    return VocabDetails(
        **vocab,
        related_vocabs=await database.fetch_all(query_vocabs))
