from pymysql import IntegrityError
import time
import sqlalchemy
from ..models.user import User, UserAuth, UserIn
from ..db import database, tbl_users
from fastapi import HTTPException, status


async def get_user_by_id(id: int) -> User:

    query = tbl_users.select(tbl_users.c.user_id == id)

    db_user = await database.fetch_one(query)

    if db_user:
        return User(
            user_id=db_user["user_id"],
            name=db_user["name"],
            isAdmin=db_user["is_admin"])
    else:
        return None



async def get_user_auth_by_name(username: str) -> UserAuth:

    query = tbl_users.select(tbl_users.c.name == username)

    db_user = await database.fetch_one(query)

    if db_user:
        return UserAuth(
            user_id=db_user["user_id"],
            name=db_user["name"],
            password_hash=db_user["pw_hash"],
            isAdmin=db_user["is_admin"])
    else:
        return None


async def add_user(user: UserIn):

    query = tbl_users.insert().values(
        name=user.name,
        pw_hash=user.password_hash,
        is_admin=user.isAdmin)

    try:
        last_record_id = await database.execute(query)
    except IntegrityError:
        raise HTTPException(status.HTTP_409_CONFLICT, detail="User already exists")
        
    return await get_user_by_id(last_record_id)


