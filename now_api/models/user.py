from pydantic import BaseModel


class UserBase(BaseModel):
    name:str
    isAdmin:bool=False

class UserIn(UserBase):
    password_hash: str

class User(UserBase):
    user_id: int

class UserAuth(User):
    password_hash: str

    def toUser(self):
        return User(**self.dict())

class UserToken(BaseModel):
    name:str
    password_hash: str