from pydantic import BaseModel

class VocabRelation(BaseModel):
    rel_id: int
    vocab_id: int
    related_vocab_id: int
