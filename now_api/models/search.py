from pydantic import BaseModel
from .vocabulary import Vocab


class SearchResult(BaseModel):
    query:str
    vocabs:list[Vocab]