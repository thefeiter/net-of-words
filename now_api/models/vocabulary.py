from pydantic import BaseModel


class VocabIn(BaseModel):
    primary: str
    secondary: str
    is_category: bool = False


class Vocab(VocabIn):
    vocab_id: int


class VocabDetails(Vocab):
    related_vocabs: list[Vocab]


class VocabBatchResult(BaseModel):
    sucess_count: int
    conflict_count: int
    success_vocabs: list[Vocab]
    conflict_vocabs: list[VocabIn]
