from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .config import settings
from .db import database, tbl_users
import hashlib
import sqlalchemy

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:9000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],#TODO insert origins here
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



@app.on_event("startup")
async def startup():
    await database.connect()

    stmt = tbl_users.select().where(tbl_users.c.name == "root")
    rootuser = await database.fetch_one(stmt)

    if not rootuser:
        if not settings.root_password:
            raise Exception("Please set environment variable ROOT_PASSWORD on first startup!")

        root_pw_hash = hashlib.sha256(settings.root_password.encode("UTF-8")).hexdigest()

        query = tbl_users.insert().values(
            name="root",
            pw_hash=root_pw_hash)

        await database.execute(query)

        stmt = tbl_users.update().where(tbl_users.c.name == "root").values(is_admin=True)
        update_count = await database.execute(stmt)

        if not update_count:
            raise Exception("Error on initial application setup")


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()
