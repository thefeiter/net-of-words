import databases
import sqlalchemy as sa

from . import settings

DATABASE_URL = settings.database_url

database = databases.Database(DATABASE_URL)

metadata = sa.MetaData()

tbl_vocabs = sa.Table(
    "vocabulary",
    metadata,
    sa.Column("vocab_id", sa.Integer, primary_key=True),
    sa.Column("primary", sa.String(length=255), nullable=False),
    sa.Column("secondary", sa.String(length=255), nullable=False),
    sa.Column("is_category", sa.Boolean, server_default=sa.text("0")),
    sa.UniqueConstraint(
        'primary', 'secondary', name='unique_vocab_combination'),
)

sa.Index('prim_ftxt', tbl_vocabs.c.primary, mysql_prefix='FULLTEXT')
sa.Index('sec_ftxt', tbl_vocabs.c.secondary, mysql_prefix='FULLTEXT')

tbl_users = sa.Table(
    "users",
    metadata,
    sa.Column("user_id", sa.Integer, primary_key=True),
    sa.Column("name", sa.String(length=255), nullable=False, unique=True),
    sa.Column("pw_hash", sa.String(length=255), nullable=False),
    sa.Column("created", sa.TIMESTAMP, nullable=False,
                      server_default=sa.text('CURRENT_TIMESTAMP')),
    sa.Column("auth_token", sa.String(length=64)),
    sa.Column("auth_token_valid", sa.TIMESTAMP),
    sa.Column("is_admin", sa.Boolean, server_default=sa.text("0"))
)

tbl_vocab_relations = sa.Table(
    "vocab_relations",
    metadata,
    sa.Column("rel_id", sa.Integer, primary_key=True),
    sa.Column("vocab_id", sa.ForeignKey("vocabulary.vocab_id", ondelete="CASCADE"), nullable=False,),
    sa.Column("related_vocab_id", sa.ForeignKey("vocabulary.vocab_id", ondelete="CASCADE"), nullable=False),
    sa.UniqueConstraint(
        'vocab_id', 'related_vocab_id', name='unique_vocab_relation'),
)



engine = sa.create_engine(
    DATABASE_URL
)
metadata.create_all(engine)
