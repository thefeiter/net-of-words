let api_url;

const hostname = window && window.location && window.location.hostname;
if (hostname === 'now.marvinfeiter.de') {
    api_url = 'https://api.now.marvinfeiter.de';
} else {
    api_url = "http://192.168.0.88:8000";
}


const VocabItem = ({ primary, secondary, vocab_id }) => `
    <div class="vocabulary listitem" vocab_id="${vocab_id}">
        <a href="javascript:void(0);" class="vocab_item_link">
            <p>${primary}-${secondary}</p>
        </a>
    </div>
`;


function load_templates(onComplete) {
    $.ajax({
        url: "/include/templates.html",
        success: function (data) {
            data = $('<html />').html(data);


            $('#topnav').html(data.find("#nav").html())
            $('#footer').html(data.find("#footer").html())

            addNavCallbacks();
            if (onComplete) {
                onComplete();
            }
        },
    });
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
    return false;
};

function fetch_search() {
    const query = $("#search-query").val();
    return $.ajax({
        url: api_url + "/search/?" + $.param({ query: query }),
        method: "GET",
    });
}


function check_auth_token(token) {

    return $.ajax({
        url: `${api_url}/user/`,
        method: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", 'Token ' + token);
        },/*
        statusCode: {
            401: login_redirect
        }*/
    });
}

async function digestMessage(message) {
    const msgUint8 = new TextEncoder().encode(message);                           // encode as (utf-8) Uint8Array
    const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8);           // hash the message
    const hashArray = Array.from(new Uint8Array(hashBuffer));                     // convert buffer to byte array
    const hashHex = hashArray.map((b) => b.toString(16).padStart(2, '0')).join(''); // convert bytes to hex string
    return hashHex;
}

function snackbarMessage(text) {

    $('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', '/snackbar.css'));

    if (!$('#snackbar').length) {
        $('body').append('<div id="snackbar"></div>');
    }
    sb = $("#snackbar")
    sb.html(text)
    sb.toggleClass("show")
    setTimeout(function () { sb.toggleClass("show") }, 3000);
}

function  addNavCallbacks() {
    $("#nav-vis").click(function (e) {
        e.preventDefault();
        $.ajax({
            url: `${api_url}/relnet/`,
            method: "GET"
        }).done(()=>{
            window.location="/vis/relations-primary.html"
        });
    })
}