

function fetch_token(username, password_hash) {


    return $.ajax({
        contentType: 'application/json',
        url: `${api_url}/user/token/?${$.param({ username: username, password_hash: password_hash })}`,
        method: "GET",
    });

}


$(document).ready(function () {

    load_templates();

    $("#content").find("form").on("submit", function (event) {
        event.preventDefault();


        username = $(this).find("#username").val()
        password = $(this).find("#password").val()

        digestMessage(password).then(function (password_hash) {
            fetch_token(username, password_hash).done(function (data, status) {
                
                if (status == 'success') {
                    $.cookie('now_access_token', data, { path: "/" });
                    window.location.href = '../';
                }
            }).fail((jqXHR, textStatus, errorThrown)=>{
                if (jqXHR.status == 401) {
                    snackbarMessage("Wrong username or password")
                }
            })
        })
    });

});