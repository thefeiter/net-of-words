var vocab_id = undefined;

function updateVocabDetails(data) {
    vocab_id = data.vocab_id
    $('#vocab-title').html(data.primary + " - " + data.secondary);
    $('#related-vocabulary').html(data.related_vocabs.map(VocabItem).join(''));

    history.pushState({}, null, "/vocabulary/?" + $.param({ vocab_id: data.vocab_id }));
}

function postNewVocabRelation(vocab_id, related_vocab_id, token) {

    return $.ajax({
        url: `${api_url}/vocabs/${vocab_id}/relations/${related_vocab_id}/`,
        method: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", 'Token ' + token);
        },
    });
}


function getVocabDetails(vocab_id) {
    return $.ajax({
        url: api_url + "/vocabs/" + vocab_id + "/",
        method: "GET",
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 404) {
                snackbarMessage("This vocab doesn't exist")
            }
        }
    });
}

$(document).ready(function () {

    load_templates(() => {
        $("#nav-random").click(function (e) {
            e.preventDefault();
            getVocabDetails("random").done(updateVocabDetails)
        })

        $("#nav-unrelated").click(function (e) {
            e.preventDefault();
            getVocabDetails("random_unrelated").done(updateVocabDetails)
        })
    });


    vocab_id = getUrlParameter("vocab_id");
    var token = $.cookie("now_access_token");
    dialog = $("#dialog-form").dialog({
        autoOpen: false,
        height: 400,
        width: 350,
        modal: true,
        buttons: {
            Cancel: function () {
                dialog.dialog("close");
            }
        }
    });

    if (vocab_id) {
        getVocabDetails(vocab_id).done(updateVocabDetails)
    } else {
        window.location.href = '/';
    }

    $("#related-vocabulary").on("click", "div.vocabulary.listitem", function () {
        getVocabDetails($(this).attr("vocab_id")).done(updateVocabDetails)
    });

    check_auth_token(token).done(function () {
        $("#add-vocab-relation").show()

        dialog.find("form").on("submit", function (event) {
            event.preventDefault();
            fetch_search().done(function (data) {
                dialog.find("#search-result-vocabs").html(data.vocabs.map(VocabItem).join(''))
            });
        });

        dialog.on("click", "div.vocabulary.listitem", function () {
            postNewVocabRelation(vocab_id, $(this).attr("vocab_id"), token).done(function () {
                dialog.dialog("close")
                getVocabDetails(vocab_id).done(updateVocabDetails)
                snackbarMessage("Relation added")
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 409) {
                    dialog.dialog("close")
                    snackbarMessage("Relation already exists")
                } else if (jqXHR.status == 400) {
                    dialog.dialog("close")
                    snackbarMessage("Cannot relate to oneself")
                }
            });
        });


        $("#add-vocab-relation").click(function () {
            dialog.dialog("open");
        })
    })



});