
function update_search_results(data) {
    $('#search-result-vocabs').html(data.vocabs.map(VocabItem).join(''));
}


$(document).ready(function () {

    load_templates();

    $('#search').on("submit", function (event) {
        event.preventDefault();
        fetch_search().done(update_search_results);
    });



    $(document).on("click", "a.vocab_item_link" , function() {
        window.location.href = "/vocabulary/?" + $.param({ vocab_id: $(this).parent().attr("vocab_id") });
    });

});