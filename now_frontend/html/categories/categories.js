var token;

function getCategories() {

    $.ajax({
        url: api_url + "/vocabs/?only_categories=true",
        method: "GET",
        error: function () {

        },
        success: function (data) {
            $('#categories').html(data.map(VocabItem).join(''));
        },
    });

}

function postNewCategory(dialog) {

    primary = dialog.find("form").find("#primary").val()
    secondary = dialog.find("form").find("#secondary").val()

    return $.ajax({
        url: `${api_url}/vocabs/`,
        method: "POST",
        contentType: 'application/json',
        data: JSON.stringify({ primary: primary, secondary: secondary , is_category: true}),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", 'Token ' + token);
        },
    });
}

$(document).ready(function () {

    load_templates(()=>{});

    token = $.cookie("now_access_token");
    dialog = $("#dialog-form").dialog({
        autoOpen: false,
        height: 400,
        width: 350,
        modal: true,
        buttons: {
            Cancel: function () {
                dialog.dialog("close");
            },
            Submit: function () {
                postNewCategory(dialog).done(function () {
                    dialog.dialog("close");
                    getCategories();
                    snackbarMessage("Category created")
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status == 409) {
                        dialog.dialog("close")
                        snackbarMessage("Category already exists")
                    }else if (jqXHR.status == 400) {
                        dialog.dialog("close")
                        snackbarMessage("Category may not be empty")
                    }else{
                        dialog.dialog("close")
                        snackbarMessage("Something ain't right")
                    }
                })
            }
        }
    });

    check_auth_token(token).done(function () {
        $("#add-category").show()
        $("#add-category").click(function () {
            dialog.dialog("open");
        })
    });
    
    getCategories();

    $("#categories").on("click", "div.vocabulary.listitem", function () {
        window.location.href = "/vocabulary/?" + $.param({ vocab_id: $(this).attr("vocab_id") });
    });


});