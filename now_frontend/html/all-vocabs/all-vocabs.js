var token;

function getVocabs() {

    $.ajax({
        url: api_url + "/vocabs/",
        method: "GET",
        error: function () {

        },
        success: function (data) {
            $('#vocabs').html(data.map(VocabItem).join(''));
        },
    });

}

function postNewVocab(dialog) {

    primary = dialog.find("form").find("#primary").val()
    secondary = dialog.find("form").find("#secondary").val()

    return $.ajax({
        url: `${api_url}/vocabs/`,
        method: "POST",
        contentType: 'application/json',
        data: JSON.stringify({ primary: primary, secondary: secondary }),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", 'Token ' + token);
        },
    });
}

$(document).ready(function () {

    load_templates();

    token = $.cookie("now_access_token");
    dialog = $("#dialog-form").dialog({
        autoOpen: false,
        height: 400,
        width: 350,
        modal: true,
        buttons: {
            Cancel: function () {
                dialog.dialog("close");
            },
            Submit: function () {
                postNewVocab(dialog).done(function () {
                    dialog.dialog("close");
                    getVocabs();
                    snackbarMessage("Vocab created")
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status == 409) {
                        dialog.dialog("close")
                        snackbarMessage("Vocab already exists")
                    }else if (jqXHR.status == 400) {
                        dialog.dialog("close")
                        snackbarMessage("Vocab may not be empty")
                    }else{
                        dialog.dialog("close")
                        snackbarMessage("Something ain't right")
                    }
                })
            }
        }
    });


    $("#vocabs").on("click", "div.vocabulary.listitem", function () {
        window.location.href = "/vocabulary/?" + $.param({ vocab_id: $(this).attr("vocab_id") });
    });

    check_auth_token(token).done(function () {
        $("#add-vocab").show()



        $("#add-vocab").click(function () {
            dialog.dialog("open");
        })
    });

    getVocabs();

});