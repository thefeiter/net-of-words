import pandas as pd
import requests
# read by default 1st sheet of an excel file
vocabs = pd.read_excel('data/Vocab Collection.xlsx')
 
#print(vocabs)

vocabs=[{"primary":vocab[1][0], "secondary":vocab[1][1]} for vocab in vocabs.iterrows()]

username=input("user: ")
password=input("password: ")

res=requests.post(
    "http://localhost:8000/vocabs/batch/",
    auth=(username, password),
    json=vocabs
)

print(res.json())